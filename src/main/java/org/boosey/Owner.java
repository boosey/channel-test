package org.boosey;

import io.smallrye.reactive.messaging.annotations.Channel;
import io.smallrye.reactive.messaging.annotations.Emitter;
import javax.inject.Inject;

import org.eclipse.microprofile.reactive.messaging.Incoming;

public class Owner {

    @Inject @Channel("ownercreated") public Emitter<String> ownerCreatedChannel;
    public String name;
    public String unit;

    public void persist() { 
        this.ownerCreatedChannel.send("a message message");
    }
}
